import Vue from 'vue'
import VueRouter from 'vue-router'
const Welcome = () => import('../components/welcome.vue')
const Login = () => import('../components/Login.vue')
const Home = () => import('../views/Home')
const user = () => import('../components/users')
const Roles = () => import('../components/Roles')
const Rights = () => import('../components/Rights')
const GoodsCate = () => import('../components/GoodsCate')
const GoodsParams = () => import('../components/GoodsParams')
const GoodsList = () => import('../components/GoodsList')
const Add = () => import('../components/add')
const Orders = () => import('../components/Orders')
const Reports = () => import('../components/Reports')
Vue.use(VueRouter)

const routes = [
  { path: '/backstage/', redirect: '/backstage/login' },
  { path: '/backstage/login', component: Login },
  {
    path: '/backstage/home',
    component: Home,
    children: [
      { path: '/backstage/home', component: Welcome },
      { path: '/backstage/users', component: user },
      { path: '/backstage/roles', component: Roles },
      { path: '/backstage/rights', component: Rights },
      { path: '/backstage/categories', component: GoodsCate },
      { path: '/backstage/params', component: GoodsParams },
      { path: '/backstage/goods', component: GoodsList },
      { path: '/backstage/goods', component: GoodsList },
      { path: '/backstage/goods/add', component: Add },
      { path: '/backstage/orders', component: Orders },
      { path: '/backstage/reports', component: Reports }
    ]
  }
]

const router = new VueRouter({
  routes
})
// 路由前置守卫
router.beforeEach((to, from, next) => {
  const tokenStr = window.sessionStorage.getItem('token')
  if (tokenStr || to.path === '/backstage/login') {
    next()
  } else {
    return next('/backstage/login')
  }
})
export default router

import { request } from './request'

export function loginData () {
  return request({
    url: 'login',
    method: 'post'
  })
}

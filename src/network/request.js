import axios from 'axios'

export function request (config) {
  // eslint-disable-next-line no-unused-vars
  const instant = axios.create({
    baseURL: 'http://127.0.0.1:8888/api/private/v1/',
    timeout: 5000
  })
  // 请求拦截
  // eslint-disable-next-line no-unused-expressions
  instant.interceptors.request.use(config => {

  })
  return instant(config)
}

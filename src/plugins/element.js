import Vue from 'vue'
import {
  Button, Form, FormItem, Input, Container, Header, Aside, Main,
  Message, Menu, Submenu, MenuItemGroup, MenuItem, Dialog, Breadcrumb,
  BreadcrumbItem, Card, Row, Col, Table, TableColumn, Pagination, Switch,
  MessageBox, Tag, Divider, Checkbox, CheckboxGroup, Tree, Select, Option,
  Tooltip, Cascader, Alert, Tabs, TabPane, Steps, Step, Upload, Timeline,
  TimelineItem
} from 'element-ui'

Vue.use(Button).use(Form).use(FormItem).use(Input).use(Container)
  .use(Header).use(Aside).use(Main).use(Menu).use(Submenu).use(MenuItemGroup)
  .use(MenuItem).use(Dialog).use(Breadcrumb).use(BreadcrumbItem).use(Card)
  .use(Row).use(Col).use(Table).use(TableColumn).use(Pagination).use(Switch)
  .use(Tag).use(Divider).use(Checkbox).use(CheckboxGroup).use(Tree).use(Select)
  .use(Option).use(Tooltip).use(Cascader).use(Alert).use(Tabs).use(TabPane)
  .use(Steps).use(Step).use(Upload).use(Timeline).use(TimelineItem)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox
